import { LitElement, html, css } from "lit";

export class PostUI extends LitElement {

  static get styles (){
    return css `
    
    #title {
      color:blue;
      font-weight:bold;     
    }
    `;
  }
    
  static get properties() {
    return {
      post: { type: Object },
    };
  }
  
  render() {
    return html`
      <p id="title">${this.post?.title}</p>
      `;
      
      
  }
  
}

customElements.define("post-ui", PostUI);
