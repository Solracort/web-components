import { LitElement, css, html } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import "./../ui/post.ui";
import { DeletePostUseCase } from "../usecases/delete-post.usecase";


export class PostsComponent extends LitElement {
  
  static get styles (){
    return css `
    .container{
      display:flex;
      width: 100%
    }
    #mypostscontainer {
      display:flex;
      justify-content: space-between;
      flex-direction:column;
      max-width:30%;
      margin: 1%;
      overflow:hidden;
      color:red;
      background-color: blanchedalmond;
      list-style: none;
      
    }
    #myselectedpost {
     
      width: 70%;
      height: 80%;
      margin: 1%;
      overflow:hidden;
      color: red;
      background-color: blanchedalmond;
    }
    #choice {
      display:flex
      flex-direction:column
      align-items: justify-content;
    }
    
    #myselectedpost label {
      width:30%;
      text-align: right;
      margin-right: 1rem;
    }
    #myselectedpost input {
      width:60%;
      padding: 0.5rem;
      margin-bottom: 1rem;
      box-sizing: border-box;
    }
    #field1, #field2, #field3{
      display:flex;
      justify-content: center;
    }
    #field3 button{
      margin:1em;
    }
    #addbutton {
      width: fit-content;
      margin-left:auto;
      padding:0.5rem;
    }
    #update {
      display: none;
    }
    #delete {
      display: none;
    }
    ul li {
      list-style: none;
    }
    h1 {
      margin:1%;
    }
    button {
      margin-bottom: 2%;
      padding:1em;
      border-radius: 0.25em;
      margin-x : 1%;
    }
    `;
  }

  static get properties() {
    return {
      posts: { type: Array },
      titleinput: {type: Object},
      contentinput: {type:Object},
      postIdselected: {type: Number}
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.posts = await AllPostsUseCase.execute();
    //preparing a copy from the original to use add and delete buttons
    this.myPostsList = this.posts;
    

    const addButton = this.shadowRoot.querySelector("#addbutton");
    const updateButton = this.shadowRoot.getElementById("update");
    const deleteButton = this.shadowRoot.getElementById("delete");
    const cancelButton = this.shadowRoot.getElementById("cancel");
    // Adding a default hidden style to buttons
    updateButton.style.display = "none";
    deleteButton.style.display = "none";

    this.titleinput = this.shadowRoot.querySelector('#title');
    this.contentinput = this.shadowRoot.querySelector('#content');


    addButton.addEventListener("click", () => {
      updateButton.style.display === "none" ? updateButton.style.display= "block" : updateButton.style.display="none";
      deleteButton.style.display === "none" ? deleteButton.style.display= "block" : deleteButton.style.display="none";
      
    });
    cancelButton.addEventListener("click", ()=> {
      this.titleinput.value="";
      this.contentinput.value="";
    });

    deleteButton.addEventListener("click", () => {
      this.deletePost(this.titleinput.value);
    });
    updateButton.addEventListener("click", () => {
      this.updatePost(this.titleinput.value, this.contentinput.value);
    });

  }
  updateInput(title , content, id) {
    this.titleinput.value = title;
    this.contentinput.value = content;
    this.postIdselected = id;
  };
  async deletePost(title) {
    const post2delete= this.myPostsList.find(post=> post.title === title);
        
    this.myPostsList = await DeletePostUseCase.execute(this.myPostsList,post2delete.id);
    const titledeleted = this.shadowRoot.querySelector('#title');
    const contentdeleted = this.shadowRoot.querySelector('#content');
    titledeleted.value = "";
    contentdeleted.value = "";
    this.requestUpdate();
  }
  updatePost(title, content) {
    
    // We prepare the new values and look for the post to change
    const newPost = {id: this.postIdselected, title: title, content:content}
    const index = this.myPostsList.findIndex(post=> post.id === this.postIdselected)
    
    // If we find the post we change the values
    if (index !== -1){
      this.myPostsList[index] = newPost;
    }
    
    // We delete the inputs values
    const titleupdated = this.shadowRoot.querySelector('#title');
    const contentupdated = this.shadowRoot.querySelector('#content');
    titleupdated.value = "";
    contentupdated.value = "";
    this.requestUpdate();
  }
  
  render() {
    return html`
      <div class = "container">
          <div id="mypostscontainer">
          <button id="addbutton">Add</button>  
          <h1>Posts List</h1>
          <ul>
            ${this.myPostsList?.map(
              (post) => html`<li @click="${() => 
                              this.updateInput(post.title, post.content, post.id)}">
              <post-ui .post="${post}"></post-ui>
              </li>`
            )}
          </ul>
          </div>
          <!-- second item  -->
          <div id="myselectedpost">
            <h1>Posts Detail</h1>
            <div id="choice">
                <div id="field1">
                    <label for="title">Title</label>
                    <input name="title" type="text" id="title">
                </div>
                <div id="field2">
                    <label for="content">Content</label>
                    <input name="content" type="text" id="content">
                </div>
                <div id="field3">
                    <button id="cancel">Cancel</button> 
                    <button id="update">Update</button> 
                    <button id="delete">Delete</button>
                </div>
            </div>
            
          </div> 
      </div>     
    `;
    }  
}
customElements.define("genk-posts", PostsComponent);
